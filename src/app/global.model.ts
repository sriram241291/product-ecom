export class GlobalModel {
	name: string;
	description: string;
	price: number;
	id: string;
	brand: string;
}
