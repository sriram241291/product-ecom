import { takeUntil } from 'rxjs/operators';
import { GlobalService } from './../global.service';
import { Component, OnInit } from '@angular/core';
import { snackbar } from '../utilities/animations';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss'],
  animations: [snackbar]
})
export class SnackbarComponent implements OnInit {

  snackbarShow = false;
  message = "";
  timeOut: any;
  unsubscribeAll = new Subject();

  constructor(private _globalservice: GlobalService) {
    this._globalservice.getSnackbarMsg()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((res) => {
        if (res[0])
          this.showSnackBar(res[1])
      })
  }

  ngOnInit() {
  }

  showSnackBar(message: string) {
    this.message = message
    if (this.timeOut)
      clearTimeout(this.timeOut)
    this.snackbarShow = true;
    this.timeOut = setTimeout(() => {
      this.snackbarShow = false
    }, 2000);
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.unsubscribe();
  }

}
