import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
	itemsPresent;
	showEmptyCart : boolean = false;
	showCart : boolean = false;
	constructor(private _cartservice: CartService) { }

	ngOnInit() {
		this.itemsPresent = this._cartservice.getCartItems();
		if(this.itemsPresent == 0) {
			this.showEmptyCart = true;
			this.showCart = false;
		} else {
			this.showEmptyCart= false;
			this.showCart = true;
		}
	}

}
