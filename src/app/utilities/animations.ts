import { trigger, transition, style, animate, query, group } from '@angular/animations';
export const snackbar = trigger('snackbar', [
    transition('void => *', [
        style({ transform: 'translateX(-25%)' }),
        animate(300)
    ]),
    transition('* => void', [
        animate(300, style({ transform: 'translateX(-100%)' }))
    ])
])

export const slider = trigger('slider', [
    transition(":increment", group([
        query(':enter', [
            style({
                left: '100%'
            }),
            animate('0.5s ease-out', style('*'))
        ]),
        query(':leave', [
            animate('0.5s ease-out', style({
                left: '-100%'
            }))
        ])
    ])),
    transition(":decrement", group([
        query(':enter', [
            style({
                left: '-100%'
            }),
            animate('0.5s ease-out', style('*'))
        ]),
        query(':leave', [
            animate('0.5s ease-out', style({
                left: '100%'
            }))
        ])
    ])),
])
