import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
	host: {
		'(document:click)': 'onClick($event)',
	}
})
export class HeaderComponent implements OnInit {
	@ViewChild("navmenu") navmenu: ElementRef;
	//Will use this later when using from other components
	@Output() islogin = new EventEmitter<boolean>();
	hideMenu: boolean = true;
	CartItems: number;

	constructor(private _cartservice : CartService) { }

	ngOnInit() {
		this.CartItems = this._cartservice.getCartItems().length;
	}

	toggleNav() {
		this.hideMenu = !this.hideMenu;
	}

	showLogin() {
		this.islogin.emit(true);
	}

	onClick(event) {
		if (!this.navmenu.nativeElement.contains(event.target)) {
			this.hideMenu = true;
		}
	}

}
