import { Injectable } from '@angular/core';
import { GlobalModel } from './global.model';
import { GlobalService } from './global.service';

@Injectable()

export class CartService {
	items: GlobalModel[] = [];
	
	constructor(private _globalservice: GlobalService) { }
	
    addToCart(x: GlobalModel) {
        if(this.items.indexOf(x) == -1) {
            this.items.push(x);
            return this.items;
        } else {
            this._globalservice.showSnackBar(true, x.name + ',' + 'is already in your cart');
        }
	}
	
    getCartItems() {
        return this.items;
    }
}