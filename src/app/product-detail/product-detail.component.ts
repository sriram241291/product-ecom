import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { GlobalModel } from '../global.model';
import { GlobalService } from '../global.service';
import { CartService } from '../cart.service';
import { slider } from '../utilities/animations';

@Component({
	selector: 'app-product-detail',
	templateUrl: './product-detail.component.html',
	styleUrls: ['./product-detail.component.scss'],
	animations: [slider]
})

export class ProductDetailComponent implements OnInit {
	productID: any;
	products: any;
	selectedProduct: any;
	showPrevIcon: boolean = false;
	showNextIcon: boolean = true;
	constructor(private activeRoute: ActivatedRoute,
		private _globalservice: GlobalService,
		private _cartservice: CartService,
		private router: Router) {
		this.productID = this.activeRoute.snapshot.params.id;
	}

	ngOnInit() {
		this.activeRoute.paramMap.subscribe(params => {
			this.productID = params.get('id');
		});
		let products = this._globalservice.products;
		this.selectedProduct = products.find(p => p.id == this.productID);
	}

	addToCart(x: GlobalModel): void {
		this._globalservice.showSnackBar(true, x.name + ',' + 'was succesfully added to your cart!');
		this._cartservice.addToCart(x);
		// this.router.navigateByUrl('/cart');
	}

	buyItem(x: GlobalModel): void {
		this._globalservice.showSnackBar(true, x.name + ',' + 'added to cart and redirecting to payment page');
		this._cartservice.addToCart(x);
		this.router.navigateByUrl('/cart');
	}

	private _images: string[] = ['https://images-na.ssl-images-amazon.com/images/I/61ers6OzvUL._SL1024_.jpg',
		'https://images-na.ssl-images-amazon.com/images/I/51nKJxw6dfL._SL1024_.jpg',
		'https://images-na.ssl-images-amazon.com/images/I/71a4xG1nQAL._SL1500_.jpg'
	];
	selectedIndex: number = 0;

	get images() {
		return [this._images[this.selectedIndex]];
	}

	previous() {
		this.selectedIndex = Math.max(this.selectedIndex - 1, 0);
		if(this.selectedIndex == 0) {
			this.showPrevIcon = false;
			this.showNextIcon = true;
		} else {
			this.showNextIcon = true;
		}
	}

	next() {
		this.selectedIndex = Math.min(this.selectedIndex + 1, this._images.length - 1);
		if(this.selectedIndex <= this._images.length) {
			this.showPrevIcon = true;
			this.showNextIcon = true;
		}
		if(this.selectedIndex == this._images.length -1) {
			this.showNextIcon = false;
		}
	}

}
